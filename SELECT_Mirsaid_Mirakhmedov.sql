/* Which staff members made the highest revenue for each store and deserve a bonus for the year 2017? */


-- first solution
SELECT s.store_id, s.employee_id, s.employee_name, p.revenue
FROM staff s
INNER JOIN (
  SELECT store_id, MAX(revenue) AS revenue
  FROM paychecks
  WHERE YEAR(date) = 2017
  GROUP BY store_id
) p ON s.store_id = p.store_id AND s.revenue = p.revenue;

-- Second solution

SELECT s.store_id, s.employee_id, s.employee_name, p.revenue
FROM staff s
INNER JOIN (
  SELECT store_id, MAX(revenue) AS revenue
  FROM paychecks
  WHERE YEAR(date) = 2017
  GROUP BY store_id
) p ON s.store_id = p.store_id AND s.revenue = p.revenue;

/* Which five movies were rented more than the others, and what is the expected age of the audience for these movies? */

-- first solution
SELECT m.movie_title, COUNT(r.rental_id) AS rental_count, AVG(a.age) AS expected_age
FROM rentals r
JOIN movies m ON r.movie_id = m.movie_id
JOIN audience a ON r.customer_id = a.customer_id
GROUP BY m.movie_title
ORDER BY rental_count DESC
LIMIT 5;

-- Second solution
SELECT m.movie_title, COUNT(r.rental_id) AS rental_count, AVG(a.age) AS expected_age
FROM rentals r
JOIN movies m ON r.movie_id = m.movie_id
JOIN audience a ON r.customer_id = a.customer_id
GROUP BY m.movie_title
HAVING COUNT(r.rental_id) > (
  SELECT COUNT(rental_id)
  FROM rentals
  GROUP BY movie_id
  ORDER BY COUNT(rental_id) DESC
  LIMIT 1
)
ORDER BY rental_count DESC
LIMIT 5;

/* Which actors/actresses didn't act for a longer period of time than the others? */

-- first solution
SELECT a.actor_id, a.actor_name
FROM actors a
LEFT JOIN acting_history ah ON a.actor_id = ah.actor_id
GROUP BY a.actor_id, a.actor_name
HAVING MAX(ah.end_date) - MIN(ah.start_date) < (
  SELECT MAX(end_date) - MIN(start_date)
  FROM acting_history
);

-- second solution
SELECT a.actor_id, a.actor_name
FROM actors a
LEFT JOIN acting_history ah ON a.actor_id = ah.actor_id
GROUP BY a.actor_id, a.actor_name
HAVING MAX(ah.end_date - ah.start_date) < (
  SELECT MAX(end_date - start_date)
  FROM acting_history
);
